# bybitexchange lightweight client library

Set up an account on bybit exchange and create the api keys.
Set the api key and the api secret in the application.properties file.

In the Java code set the url prefix in order to perform the necessary calls using the bybit client:

    // Base url to call
    String baseUrl = BybitURLEndpoints.TESTNET;

Be aware of the restrictions related the time window requests and put in sync the local machine with the server time.
To do that use the explanation as in main program:

    long delta = callServerTime(baseUrl);
    
    BybitExchangeApiClient client = new BybitExchangeApiClient(baseUrl);
    client.setTimeFixDelta(delta);

Perform the call, for example to place an order, interacting with the client.

    // Create a single order
    callPlaceOrder(client);