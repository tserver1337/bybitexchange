package com.bybitexchange;

import com.bybitexchange.client.encryption.Encryption;
import com.bybitexchange.client.keys.ApiKeyPropertyLoader;
import com.bybitexchange.client.keys.ApiKeys;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Comparator;
import java.util.TreeMap;

public class TestEncryptionMain {

	public static void main(String[] args) throws InvalidKeyException, NoSuchAlgorithmException {
		ApiKeyPropertyLoader apiKeyPropertyLoader = new ApiKeyPropertyLoader();
		ApiKeys apiKeys = apiKeyPropertyLoader.loadApiKeys();

		TreeMap map = new TreeMap<String, String>(Comparator.naturalOrder());
		map.put("symbol", "ETHUSD");
		map.put("base_price", "259");
		map.put("order_type", "Market");
		map.put("qty", "2000");
		map.put("side", "Sell");
		map.put("trigger_by", "MarkPrice");
		map.put("stop_px", "259");
		map.put("time_in_force", "GoodTillCancel");

		map.put("timestamp", String.valueOf(System.currentTimeMillis()));

		map.put("api_key", apiKeys.getApiKey());
		String signature = Encryption.getSignature(map, apiKeys.getSecret());

		System.out.println(map.toString());
		System.out.println(signature);
	}

}
