package com.bybitexchange;

import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;

public class UnirestTestMain {

	public static void main(String[] args) throws UnirestException {

		// test unirest on a simple api: https://reqres.in/
		String result = (String) Unirest.get("https://reqres.in/api/products/3")
				.asJson()
				.getBody()
				.getObject().toString();

		System.out.println(result);
	}

}
