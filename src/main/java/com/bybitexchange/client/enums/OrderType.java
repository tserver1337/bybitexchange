package com.bybitexchange.client.enums;

// Order type
// ref. https://bybit-exchange.github.io/docs/inverse/#t-enums
public enum OrderType {

    LIMIT("Limit"),
    MARKET("Market");

    private String name;

    OrderType(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return this.name;
    }

}
