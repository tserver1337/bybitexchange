package com.bybitexchange.client.enums;

// The order status enumeration
// ref. https://bybit-exchange.github.io/docs/inverse/#order-order
public enum OrderStatus {

    CREATED("Created"),
    REJECTED("Rejected"),
    NEW("New"),
    PARTIALLY_FILLED("PartiallyFilled"),
    FILLED("Filled"),
    CANCELLED("Cancelled"),
    PENDING_CANCEL("PendingCancel");

    private String statusName;

    OrderStatus(String statusName) {
        this.statusName = statusName;
    }

}
