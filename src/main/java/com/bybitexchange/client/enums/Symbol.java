package com.bybitexchange.client.enums;

// Symbol enumeration
// ref. : https://bybit-exchange.github.io/docs/inverse/#side-side
public enum Symbol {

    BTCUSD,
    ETHUSD,
    EOSUSD,
    XRPUSD;

}
