package com.bybitexchange.client.enums;

// Side
// ref. https://bybit-exchange.github.io/docs/inverse/#t-enums
public enum Side {

    BUY("Buy"),
    SELL("Sell");

    private String sideName;

    Side(String sideName) {
        this.sideName = sideName;
    }

    @Override
    public String toString() {
        return this.sideName;
    }

}
