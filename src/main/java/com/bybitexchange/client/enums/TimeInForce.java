package com.bybitexchange.client.enums;

// Time In Force
// ref. https://bybit-exchange.github.io/docs/inverse/#price-price
public enum TimeInForce {

    GOOD_TILL_CANCEL("GoodTillCancel"),
    IMMEDIATE_OR_CANCEL("ImmediateOrCancel"),
    FILL_OR_KILL("FillOrKill"),
    POST_ONLY("PostOnly");

    private String name;

    TimeInForce(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return this.name;
    }

}
