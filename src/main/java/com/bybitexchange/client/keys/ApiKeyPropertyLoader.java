package com.bybitexchange.client.keys;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/** api keys property file loader */
public class ApiKeyPropertyLoader {

    private Properties prop = new Properties();

    public ApiKeys loadApiKeys() {
        ApiKeys keys = new ApiKeys();

        try (
            InputStream input = ApiKeyPropertyLoader.class.getClassLoader().getResourceAsStream("application.properties")) {
            if (input == null) {
                System.out.println("Sorry, unable to find config.properties");
                return keys;
            }

            prop.load(input);

            keys.setApiKey(prop.getProperty("bybit.apikey"));
            keys.setSecret(prop.getProperty("bybit.secret"));
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        return keys;
    }

}
