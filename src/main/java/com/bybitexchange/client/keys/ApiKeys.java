package com.bybitexchange.client.keys;

import lombok.Data;
import lombok.ToString;

// Api keys must be created using the api management section:
//  ref. : https://testnet.bybit.com/app/user/api-management
@Data
@ToString
public class ApiKeys {

    private String apiKey;
    private String secret;

}
