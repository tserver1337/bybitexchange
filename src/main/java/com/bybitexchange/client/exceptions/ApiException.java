package com.bybitexchange.client.exceptions;

public class ApiException extends RuntimeException {

    public ApiException(String exception) {
        super(exception);
    }

}
