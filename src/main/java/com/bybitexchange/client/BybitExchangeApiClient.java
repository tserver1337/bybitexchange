package com.bybitexchange.client;

import com.bybitexchange.client.dtos.*;
import com.bybitexchange.client.encryption.Encryption;
import com.bybitexchange.client.enums.OrderStatus;
import com.bybitexchange.client.enums.Symbol;
import com.bybitexchange.client.exceptions.ApiException;
import com.bybitexchange.client.keys.ApiKeyPropertyLoader;
import com.bybitexchange.client.keys.ApiKeys;
import com.google.gson.Gson;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import lombok.Data;
import org.apache.logging.log4j.util.Strings;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Comparator;
import java.util.List;
import java.util.TreeMap;
import java.util.UUID;

import static com.bybitexchange.client.ValidationUtil.validateOrderDTO;

/**
 * Client to connect to the ByBit Apis:
 *
 * https://bybit-exchange.github.io/docs/inverse/#t-authentication
 */
public class BybitExchangeApiClient implements BybitExchangeApiInterface {

    /**
     * Default request time window size in ms
     */
    public static final int DEFAULT_TIME_WINDOW = 5000;

    /**
     * A smaller recv_window is more secure, but your request may fail if the transmission time is greater than
     * your recv_window, size in ms
     */
    public static final int RECV_WINDOW = 10000;

    private String urlPrefix;

    private long delta = 0;

    private Gson gson = new Gson();

    private ApiKeys apiKeys;

    public BybitExchangeApiClient(final String urlPrefix) {
        if (Strings.isBlank(urlPrefix)) {
            throw new ApiException("urlPrefix has not be set correctly");
        }
        this.urlPrefix = urlPrefix;

        ApiKeyPropertyLoader apiKeyPropertyLoader = new ApiKeyPropertyLoader();
        this.apiKeys = apiKeyPropertyLoader.loadApiKeys();

        System.out.println("Using api keys = " + apiKeys);
    }

    /**
     * Set the value to add to the current machine time to sync with the server
     */
    public void setTimeFixDelta(long delta) {
        this.delta = delta;
    }

    //  Alternative version 2
    //  POST /v2/private/order/create
    //  https://bybit-exchange.github.io/docs/inverse/#t-placeactive
    @Deprecated
    public OrderResponse placeOrder2(final OrderDTO orderDto) throws ApiException {
        OrderResponse ret = null;

        try {
            // check mandatory fields
            validateOrderDTO(orderDto);

            // compose the json body
            TreeMap map = new TreeMap<String, String>(Comparator.naturalOrder());

            String json = "{";

            //	Api key
            json += "\"api_key\": " + "\"" + apiKeys.getApiKey() + "\"" + ",";
            map.put("api_key", apiKeys.getApiKey());

            // Order fields
            if (orderDto.getOrderType() != null) {
                json += "\"order_type\":" + "\"" + orderDto.getOrderType() + "\"" + ",";
                map.put("order_type", orderDto.getOrderType());
            }
            if (orderDto.getQty() != null) {
                json += "\"qty\":" + "\"" + orderDto.getQty().toString() + "\"" + ",";
                map.put("qty", orderDto.getQty().toString());
            }
            if (orderDto.getQty() != null) {
                json += "\"recv_window\":" + "\"" + RECV_WINDOW + "\"" + ",";
                map.put("recv_window", "" + RECV_WINDOW);
            }
            if (orderDto.getSymbol() != null) {
                json += "\"symbol\":" + "\"" + orderDto.getSymbol() + "\"" + ",";
                map.put("symbol", orderDto.getSymbol());
            }
            if (orderDto.getSide() != null) {
                json += "\"side\":" + "\"" + orderDto.getSide() + "\"" + ",";
                map.put("side", orderDto.getSide());
            }
            if (orderDto.getTimeInForce() != null) {
                json += "\"time_in_force\":" + "\"" + orderDto.getTimeInForce() + "\"" + ",";
                map.put("time_in_force", orderDto.getTimeInForce());
            }

            // TODO: implement other fields as above

            // Timestamp
            String timestampStr = String.valueOf(System.currentTimeMillis() + delta);
            json += "\"timestamp\": " + "\"" + timestampStr + "\"" + ",";
            map.put("timestamp", timestampStr);

            // Signature
            json += "\"sign\": " + "\"" + Encryption.getSignature(map, apiKeys.getSecret()) + "\"";
            json += "}";

            System.out.println(map);
            System.out.println(json);

            HttpResponse<JsonNode> jsonResponse = Unirest.post(urlPrefix + "/v2/private/order/create")
                    .header("accept", "application/json")
                    .body(json).asJson();

            System.out.println(jsonResponse.getBody().toString());

            OrderSingleResponse orderSingleResponse = gson.fromJson(jsonResponse.getBody().toString(), OrderSingleResponse.class);

            if (orderSingleResponse != null) {
                ret = orderSingleResponse.getResult();
            }

        } catch (UnirestException e) {
            e.printStackTrace();
            throw new ApiException(e.getMessage());
        } catch (InvalidKeyException ike) {
            ike.printStackTrace();
            throw new ApiException(ike.getMessage());
        } catch (NoSuchAlgorithmException nae) {
            nae.printStackTrace();
            throw new ApiException(nae.getMessage());
        }

        return ret;
    }

    //  POST /v2/private/order/create
    //  https://bybit-exchange.github.io/docs/inverse/#t-placeactive
    public OrderResponse placeOrder(final OrderDTO orderDto) throws ApiException {
        OrderResponse ret = null;

        try {
            TreeMap map = new TreeMap<String, String>(Comparator.naturalOrder());
            map.put("api_key", apiKeys.getApiKey());
            map.put("order_type", orderDto.getOrderType());
            map.put("qty", orderDto.getQty().toString());
            map.put("recv_window", "" + RECV_WINDOW);
            map.put("side", orderDto.getSide());
            map.put("symbol", orderDto.getSymbol());
            map.put("time_in_force", orderDto.getTimeInForce());

            // Timestamp
            String timestampStr = String.valueOf(System.currentTimeMillis());
            map.put("timestamp", timestampStr);

            HttpResponse<JsonNode> jsonResponse = Unirest.post(urlPrefix + "/v2/private/order/create")
                    .header("accept", "application/json")
                    .queryString("api_key", apiKeys.getApiKey())
                    .queryString("order_type", orderDto.getOrderType())
                    .queryString("qty", orderDto.getQty().toString())
                    .queryString("recv_window", "" + RECV_WINDOW)
                    .queryString("symbol", orderDto.getSymbol())
                    .queryString("side", orderDto.getSide())
                    .queryString("time_in_force", orderDto.getTimeInForce())
                    .queryString("timestamp", timestampStr)
                    .queryString("sign", Encryption.getSignature(map, apiKeys.getSecret())).asJson();

            System.out.println(jsonResponse.getBody().toString());

            OrderSingleResponse orderSingleResponse = gson.fromJson(jsonResponse.getBody().toString(), OrderSingleResponse.class);

            if (orderSingleResponse != null) {
                ret = orderSingleResponse.getResult();
            }

        } catch (UnirestException e) {
            e.printStackTrace();
            throw new ApiException(e.getMessage());
        } catch (InvalidKeyException ike) {
            ike.printStackTrace();
            throw new ApiException(ike.getMessage());
        } catch (NoSuchAlgorithmException nae) {
            nae.printStackTrace();
            throw new ApiException(nae.getMessage());
        }

        return ret;
    }

    //  GET /v2/private/order/list
    //  https://bybit-exchange.github.io/docs/inverse/#t-getactive
    public List<OrderResponse> getOrdersSlow(Symbol symbol, OrderStatus orderStatus) throws ApiException {
        List<OrderResponse> ret = null;

        try {
            // compose the json body
            TreeMap map = new TreeMap<String, String>(Comparator.naturalOrder());
            map.put("api_key", apiKeys.getApiKey());
            map.put("order_status", orderStatus.name());

            // Timestamp
            String timestampStr = String.valueOf(System.currentTimeMillis());
            map.put("timestamp", timestampStr);

            // Symbol
            map.put("symbol", symbol.name());

            HttpResponse<JsonNode> jsonResponse = Unirest.get(urlPrefix + "/v2/private/order/list")
                    .header("Content-Type", "application/json")
                    .queryString("api_key", apiKeys.getApiKey())
                    .queryString("symbol", symbol.name())
                    .queryString("order_status", orderStatus.name())
                    .queryString("timestamp", timestampStr)
                    .queryString("sign", Encryption.getSignature(map, apiKeys.getSecret())).asJson();

            System.out.println(jsonResponse.getBody().toString());

            OrderListResponse orderListResponse = gson.fromJson(jsonResponse.getBody().toString(), OrderListResponse.class);

            if (orderListResponse != null) {
                ret = orderListResponse.getResult().getData();
            }

        } catch (UnirestException e) {
            e.printStackTrace();
            throw new ApiException(e.getMessage());
        } catch (InvalidKeyException ike) {
            ike.printStackTrace();
            throw new ApiException(ike.getMessage());
        } catch (NoSuchAlgorithmException nae) {
            nae.printStackTrace();
            throw new ApiException(nae.getMessage());
        }

        return ret;
    }

    //  GET /v2/private/order
    //  https://bybit-exchange.github.io/docs/inverse/#t-queryactive
    public List<OrderResponse> getOrdersRealTime(Symbol symbol) throws ApiException {
        List<OrderResponse> ret;

        try {
            TreeMap map = new TreeMap<String, String>(Comparator.naturalOrder());
            map.put("api_key", apiKeys.getApiKey());
            map.put("symbol", symbol.name());

            // Timestamp
            String timestampStr = String.valueOf(System.currentTimeMillis());
            map.put("timestamp", timestampStr);

            HttpResponse<JsonNode> jsonResponse = Unirest.get(urlPrefix + "/v2/private/order")
                    .header("Content-Type", "application/json")
                    .queryString("api_key", apiKeys.getApiKey())
                    .queryString("symbol", symbol.name())
                    .queryString("timestamp", timestampStr)
                    .queryString("sign", Encryption.getSignature(map, apiKeys.getSecret())).asJson();

            System.out.println(jsonResponse.getBody().toString());

            QueryActiveOrderResponse queryActiveOrderResponse = gson.fromJson(jsonResponse.getBody().toString(), QueryActiveOrderResponse.class);

            ret = queryActiveOrderResponse.getResult();

        } catch (UnirestException e) {
            e.printStackTrace();
            throw new ApiException(e.getMessage());
        } catch (InvalidKeyException ike) {
            ike.printStackTrace();
            throw new ApiException(ike.getMessage());
        } catch (NoSuchAlgorithmException nae) {
            nae.printStackTrace();
            throw new ApiException(nae.getMessage());
        }

        return ret;
    }

    //  GET /v2/private/order
    //  https://bybit-exchange.github.io/docs/inverse/#t-queryactive
    // note that if only an order_id is passed a single order will be returned
    public OrderSingleResponse getOrdersRealTime(Symbol symbol, UUID orderId) throws ApiException {
        OrderSingleResponse ret;

        try {
            TreeMap map = new TreeMap<String, String>(Comparator.naturalOrder());
            map.put("api_key", apiKeys.getApiKey());
            map.put("order_id", orderId.toString());
            map.put("symbol", symbol.name());

            // Timestamp
            String timestampStr = String.valueOf(System.currentTimeMillis());
            map.put("timestamp", timestampStr);

            HttpResponse<JsonNode> jsonResponse = Unirest.get(urlPrefix + "/v2/private/order")
                    .header("Content-Type", "application/json")
                    .queryString("api_key", apiKeys.getApiKey())
                    .queryString("order_id", orderId.toString())
                    .queryString("symbol", symbol.name())
                    .queryString("timestamp", timestampStr)
                    .queryString("sign", Encryption.getSignature(map, apiKeys.getSecret())).asJson();

            System.out.println(jsonResponse.getBody().toString());

            ret = gson.fromJson(jsonResponse.getBody().toString(), OrderSingleResponse.class);

        } catch (UnirestException e) {
            e.printStackTrace();
            throw new ApiException(e.getMessage());
        } catch (InvalidKeyException ike) {
            ike.printStackTrace();
            throw new ApiException(ike.getMessage());
        } catch (NoSuchAlgorithmException nae) {
            nae.printStackTrace();
            throw new ApiException(nae.getMessage());
        }

        return ret;
    }

    //  POST /v2/private/order/cancel
    //  https://bybit-exchange.github.io/docs/inverse/#t-cancelactive
    public OrderResponse cancelActiveOrder(Symbol symbol, UUID orderId) throws ApiException {
        OrderResponse ret = null;

        try {
            TreeMap map = new TreeMap<String, String>(Comparator.naturalOrder());
            map.put("api_key", apiKeys.getApiKey());
            map.put("order_id", orderId.toString());
            map.put("symbol", symbol.name());

            // Timestamp
            String timestampStr = String.valueOf(System.currentTimeMillis());
            map.put("timestamp", timestampStr);

            HttpResponse<JsonNode> jsonResponse = Unirest.post(urlPrefix + "/v2/private/order/cancel")
                    .header("accept", "application/json")
                    .queryString("api_key", apiKeys.getApiKey())
                    .queryString("order_id", orderId.toString())
                    .queryString("symbol", symbol.name())
                    .queryString("timestamp", timestampStr)
                    .queryString("sign", Encryption.getSignature(map, apiKeys.getSecret())).asJson();

            System.out.println(jsonResponse.getBody().toString());

            OrderSingleResponse orderResponse = gson.fromJson(jsonResponse.getBody().toString(), OrderSingleResponse.class);

            if (orderResponse != null) {
                ret = orderResponse.getResult();
            }

        } catch (UnirestException e) {
            e.printStackTrace();
            throw new ApiException(e.getMessage());
        } catch (InvalidKeyException ike) {
            ike.printStackTrace();
            throw new ApiException(ike.getMessage());
        } catch (NoSuchAlgorithmException nae) {
            nae.printStackTrace();
            throw new ApiException(nae.getMessage());
        }

        return ret;
    }

    //  POST /v2/private/order/cancelAll
    //  https://bybit-exchange.github.io/docs/inverse/#t-cancelallactive
    public List<OrderCancelledResponse> cancelAllActiveOrders(Symbol symbol) throws ApiException {
        List<OrderCancelledResponse> ret = null;

        try {
            TreeMap map = new TreeMap<String, String>(Comparator.naturalOrder());
            map.put("api_key", apiKeys.getApiKey());
            map.put("symbol", symbol.name());

            // Timestamp
            String timestampStr = String.valueOf(System.currentTimeMillis());
            map.put("timestamp", timestampStr);

            HttpResponse<JsonNode> jsonResponse = Unirest.post(urlPrefix + "/v2/private/order/cancel/cancelAll")
                    .header("accept", "application/json")
                    .queryString("api_key", apiKeys.getApiKey())
                    .queryString("symbol", symbol.name())
                    .queryString("timestamp", timestampStr)
                    .queryString("sign", Encryption.getSignature(map, apiKeys.getSecret())).asJson();

            System.out.println(jsonResponse.getBody().toString());

            CancelAllActiveOrderResponse cancelAllActiveOrderResponse = gson.fromJson(jsonResponse.getBody().toString(), CancelAllActiveOrderResponse.class);

            if (cancelAllActiveOrderResponse != null) {
                ret = cancelAllActiveOrderResponse.getResult();
            }

        } catch (UnirestException e) {
            e.printStackTrace();
            throw new ApiException(e.getMessage());
        } catch (InvalidKeyException ike) {
            ike.printStackTrace();
            throw new ApiException(ike.getMessage());
        } catch (NoSuchAlgorithmException nae) {
            nae.printStackTrace();
            throw new ApiException(nae.getMessage());
        }

        return ret;
    }

}
