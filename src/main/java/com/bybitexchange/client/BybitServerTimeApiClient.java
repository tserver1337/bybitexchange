package com.bybitexchange.client;

import com.bybitexchange.client.exceptions.ApiException;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import org.apache.logging.log4j.util.Strings;

// Client to connect to the ByBit Time Apis
public class BybitServerTimeApiClient {

    private String urlPrefix = "";

    public BybitServerTimeApiClient(String urlPrefix) {
        if (Strings.isBlank(urlPrefix)) {
            throw new ApiException("urlPrefix has not be set correctly");
        }
        this.urlPrefix = urlPrefix;
    }

    //  /v2/public/time
    public String getServerTime() throws UnirestException {
        return Unirest.get(urlPrefix + "/v2/public/time").asJson().getBody().getObject().getString("time_now");
    }

}
