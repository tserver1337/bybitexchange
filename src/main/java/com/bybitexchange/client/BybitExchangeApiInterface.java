package com.bybitexchange.client;

import com.bybitexchange.client.dtos.*;
import com.bybitexchange.client.enums.OrderStatus;
import com.bybitexchange.client.enums.Symbol;
import com.bybitexchange.client.exceptions.ApiException;

import java.util.List;
import java.util.UUID;

public interface BybitExchangeApiInterface {

    //  https://bybit-exchange.github.io/docs/inverse/#t-placeactive
    OrderResponse placeOrder(OrderDTO order) throws ApiException;

    //  https://bybit-exchange.github.io/docs/inverse/#t-getactive
    List<OrderResponse> getOrdersSlow(Symbol symbol, OrderStatus orderStatus) throws ApiException;

    //  https://bybit-exchange.github.io/docs/inverse/#t-queryactive
    List<OrderResponse> getOrdersRealTime(Symbol symbol) throws ApiException;

    // retrieve a single order based on symbol and uuid
    //  https://bybit-exchange.github.io/docs/inverse/#t-queryactive
    OrderSingleResponse getOrdersRealTime(Symbol symbol, UUID orderId) throws ApiException;

    //  https://bybit-exchange.github.io/docs/inverse/#t-cancelactive
    OrderResponse cancelActiveOrder(Symbol symbol, UUID oderId) throws ApiException;

    //  https://bybit-exchange.github.io/docs/inverse/#t-cancelallactive
    List<OrderCancelledResponse> cancelAllActiveOrders(Symbol symbol) throws ApiException;

}
