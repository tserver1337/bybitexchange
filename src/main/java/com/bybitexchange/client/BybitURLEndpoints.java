package com.bybitexchange.client;

// The Bybit endpoints available
public class BybitURLEndpoints {

    private BybitURLEndpoints() {
        throw new IllegalStateException("Utility class");
    }

    /** Testnet: https://api-testnet.bybit.com */
    public static final String TESTNET = "https://api-testnet.bybit.com";

    /** Mainnet: https://api.bybit.com or https://api.bytick.com */
    public static final String MAINNET_1 = "https://api.bybit.com";
    public static final String MAINNET_2 = "https://api.bytick.com";

}

