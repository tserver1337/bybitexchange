package com.bybitexchange.client;

import com.bybitexchange.client.dtos.OrderDTO;
import com.bybitexchange.client.exceptions.ApiException;

public class ValidationUtil {

    public static void validateOrderDTO(final OrderDTO orderDTO) {
        // verify the required parameter 'side' is set
        if (orderDTO.getSide() == null) {
            throw new ApiException("Missing the required parameter 'side' when calling orderNew(Async)");
        }

        // verify the required parameter 'symbol' is set
        if (orderDTO.getSymbol() == null) {
            throw new ApiException("Missing the required parameter 'symbol' when calling orderNew(Async)");
        }

        // verify the required parameter 'orderType' is set
        if (orderDTO.getOrderType() == null) {
            throw new ApiException("Missing the required parameter 'orderType' when calling orderNew(Async)");
        }

        // verify the required parameter 'qty' is set
        if (orderDTO.getQty() == null) {
            throw new ApiException("Missing the required parameter 'qty' when calling orderNew(Async)");
        }

        // verify the required parameter 'timeInForce' is set
        if (orderDTO.getTimeInForce() == null) {
            throw new ApiException("Missing the required parameter 'timeInForce' when calling orderNew(Async)");
        }
    }

}
