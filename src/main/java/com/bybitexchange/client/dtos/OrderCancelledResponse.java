package com.bybitexchange.client.dtos;

import lombok.Data;
import lombok.ToString;

import java.math.BigDecimal;

@Data
@ToString
public class OrderCancelledResponse {

    private String clOrdID = null;
    private BigDecimal user_id = null;
    private String symbol = null;
    private String side = null;
    private String order_type = null;
    private Double price = null;
    private String qty = null;
    private String time_in_force = null;
    private String create_type = null;
    private String cancel_type = null;
    private String order_status = null;
    private BigDecimal leaves_qty = null;
    private BigDecimal leaves_value = null;
    private String created_at = null;
    private String updated_at = null;
    private String cross_status = null;
    private long cross_seq;

}
