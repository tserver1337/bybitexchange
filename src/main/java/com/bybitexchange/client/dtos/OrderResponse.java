package com.bybitexchange.client.dtos;

import lombok.Data;
import lombok.ToString;

import java.math.BigDecimal;

@Data
@ToString
public class OrderResponse {

    private BigDecimal user_id = null;
    private String order_status = null;
    private String symbol = null;
    private String side = null;
    private String order_type = null;
    private Double price = null;
    private String qty = null;
    private String time_in_force = null;
    private String order_link_id;
    private String order_id = null;
    private String created_at = null;
    private String updated_at = null;
    private BigDecimal leaves_qty = null;
    private BigDecimal leaves_value = null;
    private BigDecimal cum_exec_qty = null;
    private BigDecimal cum_exec_value = null;
    private Double cum_exec_fee = null;
    private String reject_reason = null;

}
