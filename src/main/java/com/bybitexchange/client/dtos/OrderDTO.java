package com.bybitexchange.client.dtos;

import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;

@Builder
@Data
public class OrderDTO {

    // Side (required)
    private String side;

    // Contract type (required)
    private String symbol;

    // Active order type (required)
    private String orderType;

    // Quantity (required)
    private BigDecimal qty;

    // Time in force (required)
    private String timeInForce;

    // Order price (optional)
    private Double price;

    // Take profit price (optional)
    private Double takeProfit;

    // Stop loss price (optional)
    private Double stopLoss;

    // Reduce only (optional)
    private Boolean reduceOnly;

    // Close on trigger (optional)
    private Boolean closeOnTrigger;

    // TCustomized order ID, maximum length at 36 characters, and order ID under the same agency has to be unique. (optional)
    private String orderLinkId;

}
