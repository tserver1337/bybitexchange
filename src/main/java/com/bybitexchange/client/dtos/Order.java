package com.bybitexchange.client.dtos;

import lombok.Data;
import lombok.ToString;

import java.math.BigDecimal;

@Data
@ToString
public class Order {

    private String orderId = null;
    private BigDecimal userId = null;
    private String symbol = null;
    private String side = null;
    private String orderType = null;
    private Double price = null;
    private String qty = null;
    private String timeInForce = null;
    private String orderStatus = null;
    private Double lastExecTime = null;
    private Double lastExecPrice = null;
    private BigDecimal leavesQty = null;
    private BigDecimal cumExecQty = null;
    private BigDecimal cumExecValue = null;
    private Double cumExecFee = null;
    private String rejectReason = null;
    private String orderLinkId = null;
    private String createdAt = null;
    private String updatedAt = null;

}
