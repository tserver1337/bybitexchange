package com.bybitexchange.client.dtos;

import lombok.Data;

import java.util.List;

@Data
public class QueryActiveOrderResponse {

    private List<OrderResponse> result;

    private long rate_limit_status;
    private long rate_limit;
    private String ext_code;
    private String time_now;
    private long rate_limit_reset_ms;
    private String ret_msg;
    private String ext_info;
    private long ret_code;

}
