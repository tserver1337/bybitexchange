package com.bybitexchange.client.dtos;

import lombok.Data;
import java.util.List;

@Data
public class OrderListData {

    private List<OrderResponse> data;
    private String cursor;

}
