package com.bybitexchange.client.dtos;

import lombok.Data;

@Data
public class OrderListResponse {

    private OrderListData result;

    private long rate_limit_status;
    private long rate_limit;
    private String ext_code;
    private String time_now;
    private long rate_limit_reset_ms;
    private String ret_msg;
    private String ext_info;
    private long ret_code;

}
