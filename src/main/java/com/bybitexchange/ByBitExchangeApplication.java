package com.bybitexchange;

import com.bybitexchange.client.BybitExchangeApiClient;
import com.bybitexchange.client.BybitServerTimeApiClient;
import com.bybitexchange.client.BybitURLEndpoints;
import com.bybitexchange.client.dtos.OrderDTO;
import com.bybitexchange.client.dtos.OrderResponse;
import com.bybitexchange.client.dtos.OrderSingleResponse;
import com.bybitexchange.client.enums.*;
import com.bybitexchange.client.keys.ApiKeyPropertyLoader;
import com.bybitexchange.client.keys.ApiKeys;
import com.mashape.unirest.http.exceptions.UnirestException;

import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;

public class ByBitExchangeApplication {

	public static void main(String[] args) throws UnirestException {
		// Base url to call
		String baseUrl = BybitURLEndpoints.TESTNET;

		// Warning!:
		// Make sure to conform to the timestamp rule of verification or your request will be rejected! Timestamp rule of verification:
		// server_time - recv_window <= timestamp < server_time + 1000; server_time stands for Bybit server time, you can get it from Server Time endpoint.
//		long delta = callServerTime(baseUrl);
//
//		BybitExchangeApiClient client = new BybitExchangeApiClient(baseUrl);
//		client.setTimeFixDelta(delta);

		// Create a single order
//		callPlaceOrder(client);

		// Ex. of output:
		// {"result":{"symbol":"BTCUSD","side":"Buy","cum_exec_fee":0,"created_at":"2021-03-15T14:31:47.770Z","reject_reason":"EC_NoError","order_status":"Created","time_in_force":"ImmediateOrCancel","cum_exec_qty":0,"leaves_qty":1,"updated_at":"2021-03-15T14:31:47.770Z","user_id":147724,"price":58735,"qty":1,"last_exec_price":0,"cum_exec_value":0,"last_exec_time":0,"order_id":"c5fee1a1-aa8e-4966-96fe-255bf27cd26e","order_type":"Market","order_link_id":""},"rate_limit":100,"rate_limit_status":99,"ext_code":"","time_now":"1615818707.770835","rate_limit_reset_ms":1615818707768,"ret_msg":"OK","ext_info":"","ret_code":0}

//		List<OrderResponse> list = client.getOrdersRealTime(Symbol.BTCUSD);
//		System.out.println(list);

//		 List<OrderResponse> list = client.getOrdersSlow(Symbol.BTCUSD, OrderStatus.NEW);
//		 list.stream().forEach(o -> {
//		 	System.out.println(o);
//		 });

		 // Ex. of output:
		 // {"result":{"cursor":"mDutt2EzSct+KdSU7Yd+ASc6J4GjjcmqoPk3Fbyeb/iE0SpjRsH67afOALxHTopi","data":[{"symbol":"BTCUSD","side":"Buy","cum_exec_fee":"0.00000002","created_at":"2021-03-15T14:31:47.770Z","reject_reason":"EC_NoError","order_status":"Filled","time_in_force":"ImmediateOrCancel","cum_exec_qty":"1","updated_at":"2021-03-15T14:31:47.770Z","leaves_qty":"0","user_id":147724,"price":"58735","qty":"1","leaves_value":"0","cum_exec_value":"0.00001753","order_type":"Market","order_link_id":"","order_id":"c5fee1a1-aa8e-4966-96fe-255bf27cd26e"}]},"rate_limit":600,"rate_limit_status":599,"ext_code":"","time_now":"1615819667.257784","rate_limit_reset_ms":1615819667252,"ret_msg":"OK","ext_info":"","ret_code":0}

		// retrieve a single order based on its uuid:
//		String orderId = "68ba93cf-6408-466e-a263-4f68e515d944"; // existing order
//		OrderSingleResponse orderSingleResponse = client.getOrdersRealTime(Symbol.BTCUSD, UUID.fromString(orderId));
//		System.out.println(orderSingleResponse);

		// Ex. of output
		// {"result":{"symbol":"BTCUSD","side":"Buy","cum_exec_fee":"0.00000002","created_at":"2021-03-15T14:31:47.770140172Z","reject_reason":"EC_NoError","cancel_type":"UNKNOWN","order_status":"Filled","time_in_force":"ImmediateOrCancel","cum_exec_qty":1,"leaves_qty":0,"updated_at":"2021-03-15T14:31:47.770346Z","user_id":147724,"price":"58735","qty":1,"last_exec_price":"57024.5","leaves_value":"0","cum_exec_value":"0.00001753","last_exec_time":"1615818707.770346","ext_fields":{"o_req_num":259475},"order_type":"Market","order_link_id":"","order_id":"c5fee1a1-aa8e-4966-96fe-255bf27cd26e"},"rate_limit":600,"rate_limit_status":598,"ext_code":"","time_now":"1615820433.000643","rate_limit_reset_ms":1615820432999,"ret_msg":"OK","ext_info":"","ret_code":0}
		//OrderSingleResponse(result=OrderResponse(user_id=147724, order_status=Filled, symbol=BTCUSD, side=Buy, order_type=Market, price=58735.0, qty=1, time_in_force=ImmediateOrCancel, order_link_id=, order_id=c5fee1a1-aa8e-4966-96fe-255bf27cd26e, created_at=2021-03-15T14:31:47.770140172Z, updated_at=2021-03-15T14:31:47.770346Z, leaves_qty=0, leaves_value=0, cum_exec_qty=1, cum_exec_value=0.00001753, cum_exec_fee=2.0E-8, reject_reason=EC_NoError), rate_limit_status=598, rate_limit=600, ext_code=, time_now=1615820433.000643, rate_limit_reset_ms=1615820432999, ret_msg=OK, ext_info=, ret_code=0)

		// Note: it's possible to cancel an order that is unfilled or partially filled. Fully orders cannot be cancelled.
		// In some cases you can obtain the error message: "order not exists or too late to cancel".
		// apiClient.cancelActiveOrder(Symbol.BTCUSD, UUID.fromString(orderId));
	}

	/** call the time api client (this in order to adjust the time of the machine) */
	public static long callServerTime(final String baseUrl) throws UnirestException {
		BybitServerTimeApiClient timeApiClient = new BybitServerTimeApiClient(baseUrl);

		long localTimeNow = System.currentTimeMillis();
		String serverTimeNowStr = timeApiClient.getServerTime();

		double serverTimeDouble = Double.parseDouble(serverTimeNowStr) * 1000;
		long serverTimeNow = (long) serverTimeDouble;

		System.out.println("Server time now: " + serverTimeNow + " (" + serverTimeNowStr + ")");
		System.out.println("Local time now : " + localTimeNow);

		long delta = localTimeNow - serverTimeNow;
		System.out.println("Time Fix to apply = " + delta + " ms");

		return delta;
	}

	// place order
	public static OrderResponse callPlaceOrder(BybitExchangeApiClient apiClient) {
		OrderDTO orderDto = OrderDTO.builder()
				.side(Side.BUY.toString())
				.symbol(Symbol.BTCUSD.toString())
				.orderType(OrderType.MARKET.toString())
				.qty(BigDecimal.valueOf(1))
				.timeInForce(TimeInForce.GOOD_TILL_CANCEL.toString()).build();

		OrderResponse order = apiClient.placeOrder(orderDto);

		System.out.println(order);

		return order;
	}

}
